#!/bin/sh
echo "⏳ Testing the requirements..."
kubectl_state=$(kubectl >/dev/null 2>&1)
if [[ $? -ne 0 ]]; then
    echo "😡 kubectl is not installed on the host machine"
    exit 1
fi
echo "- kubectl   ✅"

envsubst_state=$(envsubst --version >/dev/null 2>&1)
if [[ $? -ne 0 ]]; then
    echo "😡 envsubst is not installed"
    exit 1
fi
echo "- envsubst  ✅"


git_state=$(git --version >/dev/null 2>&1)
if [[ $? -ne 0 ]]; then
    echo "😡 git is not installed"
    exit 1
fi
echo "- git       ✅"


if [[ "$1" == "version" ]]; then
  echo "1.0.0"
  exit 0
fi

function get_kube_config() {
  if [[ -z "${KUBECONFIG}" ]]; then
    FILE=${PWD}/kubeconfig.yaml
    if [[ -f "${FILE}" ]]; then
        #echo "${FILE} exists"
        export KUBECONFIG=${FILE}
    else 
      echo "😡 KUBECONFIG is empty"
      echo "😡 or ${FILE} does not exist"
      exit 1
    fi
  fi
  echo "😀 KUBECONFIG=${KUBECONFIG}"
}

if [[ "$1" == "config" ]]; then
  get_kube_config
  exit 0
fi

# TODO kubectl deploy gitlab-ci
if [[ "$1" == "deploy" ]]; then
  get_kube_config

  if [[ -z "${CI_PROJECT_PATH_SLUG}" ]]; then
    export CI_PROJECT_PATH_SLUG="only_with_gitlab_ci"
  fi

  if [[ -z "${CI_ENVIRONMENT_SLUG}" ]]; then
    export CI_ENVIRONMENT_SLUG="only_with_gitlab_ci"
  fi

  # test the variables - useful if ci
  if [[ -z "${APPLICATION_NAME}" ]]; then
    export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))
  fi

  if [[ -z "${BRANCH}" ]]; then
    export BRANCH=$(git symbolic-ref --short HEAD)
  fi

  if [[ -z "${TAG}" ]]; then
    export TAG=$(git rev-parse --short HEAD)
  fi

  #export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))
  #export BRANCH=$(git symbolic-ref --short HEAD)
  #export TAG=$(git rev-parse --short HEAD)

  # check if NAMESPACE is empty  - useful if ci (eg KUBE_NAMESPACE)
  if [[ -z "${NAMESPACE}" ]]; then
    export NAMESPACE="${APPLICATION_NAME}-${BRANCH}"
  fi

  # check if namespace exists
  kubectl describe namespace ${NAMESPACE} 
  exit_code=$?
  if [[ exit_code -eq 1 ]]; then
    echo "🖐️ ${NAMESPACE} does not exist"
    echo "⏳ Creating the namespace..."
    kubectl create namespace ${NAMESPACE}
  else 
    echo "👋 ${NAMESPACE} already exists"
  fi

  if [[ -z "${SUB_DOMAIN}" ]]; then
    if [[ -z "${HOST}" ]]; then
      echo "😡 SUB_DOMAIN and HOST are empty"
      echo "Try something like:"
      echo "export HOST=function_name.raider.eu.ngrok.io"
      echo "or"
      echo "export SUB_DOMAIN=raider.eu.ngrok.io"
      exit 1
    fi
  else
    export HOST="${APPLICATION_NAME}.${BRANCH}.${SUB_DOMAIN}"
  fi

  export CONTAINER_PORT=${CONTAINER_PORT:-8080}
  export EXPOSED_PORT=${EXPOSED_PORT:-80}

  envsubst < ~/.kgol/deploy.template.yaml | kubectl apply -n ${NAMESPACE} -f -
  
  # OMG 🙈 🙉 🙊
  kubectl create configmap ${APPLICATION_NAME}-${TAG}-golo-functions --from-file goloFunc.golo -o yaml --dry-run | kubectl apply -n ${NAMESPACE} -f -
 
  echo "🌍 http://${HOST}/exec"

  exit 0
fi

if [[ "$1" == "pod" ]]; then
  get_kube_config

  if [[ -z "${APPLICATION_NAME}" ]]; then
    export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))
  fi

  if [[ -z "${BRANCH}" ]]; then
    export BRANCH=$(git symbolic-ref --short HEAD)
  fi

  if [[ -z "${NAMESPACE}" ]]; then
    export NAMESPACE="${APPLICATION_NAME}-${BRANCH}"
  fi

  kubectl get pods -n ${NAMESPACE}
  exit 0
fi


