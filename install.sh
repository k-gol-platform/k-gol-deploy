#!/bin/sh
# https://kubernetes.io/fr/docs/reference/kubectl/overview/#exemples-cr%c3%a9er-et-utiliser-des-plugins
sudo chmod +x ./kubectl-kgol.sh

cp ./kubectl-kgol.sh ./kubectl-kgol
sudo mv ./kubectl-kgol /usr/local/bin
[ -d ~/.kgol ] || mkdir ~/.kgol 
cp ./deploy.template.yaml ~/.kgol/deploy.template.yaml
