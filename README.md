# K-Gol-Deploy

Kubectl plugin to seamless deploy Golo Functions to Kubernetes

## Install K-Gol plugin

```bash
git clone git@gitlab.com:k-gol-platform/k-gol-deploy.git
cd k-gol-deploy
./install.sh
```

> `./install.sh` will copy the kubectl plugin to `/usr/local/bin`

## Use it

First you need a Golo function. Clone the `k-gol-template` and give a name to your new project (here: `k-gol-hello`)

```bash
git clone git@gitlab.com:k-gol-platform/k-gol-template.git k-gol-hello
```

Then change directory and setup environment variables:

```bash
cd k-gol-hello

export KUBECONFIG=../wey-yu/cluster/config/k3s.yaml 
export SUB_DOMAIN=wey-yu-cluster.k3s.nimbo
```

> - Populate `KUBECONFIG` with the config file of your cluster
> - `SUB_DOMAIN` is used to build the url of your function
>    - `function_name.branch.${SUB_DOMAIN}/exec` (it will populate the `HOST` variable)
> - you can override the `HOST` variable (👋 it's under 🚧)

Edit `goloFunc.golo` (your Golo Function)

```
function goloFunc = |args| {
  return "👋 Hello Golo"
}

```

🚀 **Deploy it**:

```bash
git add .
git commit -m "👋 hello Golo"

kubectl kgol deploy
```

Check the Golo Pod:

```bash
kubectl kgol pod 
```

You'll get this:

```
NAME                         READY   STATUS    RESTARTS   AGE
k-gol-hello-9ddbb8df-mdbqs   1/1     Running   0          22m
```

🌍 Call your function

```bash
http http://k-gol-hello.master.wey-yu-cluster.k3s.nimbo/exec
```

You'll get this:

```bash
HTTP/1.1 200 OK
Content-Length: 53
Content-Type: application/json;charset=UTF-8
Date: Tue, 31 Mar 2020 08:40:13 GMT
Vary: Accept-Encoding

{
    "result": "👋 Hello Golo"
}
```


🔃 **Enable reloading k-gol function**:
You could delete the namespace or enable reloading the configMap.

If you want to enable the reloading simply install stakater reloader

```
kubectl apply -f https://raw.githubusercontent.com/stakater/Reloader/master/deployments/kubernetes/reloader.yaml
```

And when you update the function simply run again (without need to delete the namespace)
```
kubectl kgol deploy
```

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md)
